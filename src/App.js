import React, {useState} from 'react';
import './App.css';

function App() {
  const words = ["test", "bonjour"]
  const alphabet = "abcdefghijklmnopqrstuvwxyz"

  const [doneLetters , setDoneLetters] = useState([])
  const [lives, setLives] =useState(5)
  const [word, setWord] =useState(chooseRandomWord())

  function handleLetterClick(event) {
    const testedLetter = event.target.value
    tryLetter(testedLetter)
  }
  function displayButtons (){
    return alphabet.split('').map(letter => <button disabled={isButtonDisabled(letter)} key={letter} onClick={handleLetterClick} value={letter} >{letter}</button>);
  }
  function isButtonDisabled(letter) {
    if(isWon() || isLost()) {
      return true;
    }
    return IsLetterFound(letter)
  }
  function tryLetter(letter) {
    doneLetters.push(letter)
    setDoneLetters([...doneLetters])
    if(!isLetterInWord(letter)) {
      looseLife()
    }
  }

  function isLetterInWord(letter) {
    return word.indexOf(letter) !== -1
  }

  function looseLife() {
    setLives(lives-1)
  }

  function displayLife() {
    if(isLost()) {
      return "perdu!"
    }
    if(isWon()) {
      return "gagné!"
    }
    const lifeList = []
    for (let i = 0; i < lives; i++) {
      lifeList.push("<3 ")
    }
    return lifeList
  }

  function isLost() {
    return lives <= 0
  }


  function isWon() {
    return displayWord().indexOf("_ ") === -1
  }

  function randomNumber(max) {
    return Math.floor(Math.random()*(max+1))
  }

  function chooseRandomWord() {
    const i = randomNumber(words.length-1)
    return words[i]
  }

  function displayWord() {
    return word.split('').map(letter => IsLetterFound(letter)||isLost()?letter:"_ ");
  }

  function IsLetterFound(letter) {
    return doneLetters.indexOf(letter) !== -1
  }

  function displayRestartButton() {
    if(isLost() || isWon()) {
      return <button onClick={restart}>Recommencer</button>
    }
  }

  function restart() {
    setDoneLetters([])
    setWord(chooseRandomWord())
    setLives(5)
  }

  return (
    <div className="App">
      <div>
        {displayLife()}
      </div>
      <div>
        {displayWord()}
      </div>
      <div>
        {displayButtons()}
      </div>
      <div>
        {displayRestartButton()}
      </div>
    </div>
  );
}

export default App;
